import { Component, Input, OnInit } from '@angular/core';

import { ITodo } from '../../interfaces/Todo';
import { TodoStoreService } from '../../services/todo-store.service';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: ITodo;

  constructor(
    private todoStoreService: TodoStoreService
  ) { }

  ngOnInit(): void { }

  removeTodo(): void {
    this.todoStoreService.todos =
      this.todoStoreService.todos.filter(el => el !== this.todo);
  }
}
