import { Component, Input, OnInit } from '@angular/core';

import { ITodo } from '../../interfaces/Todo';
import { TodoStoreService } from '../../services/todo-store.service';

@Component({
  selector: 'app-todo-add-form',
  templateUrl: './todo-add-form.component.html',
  styleUrls: ['./todo-add-form.component.scss']
})
export class TodoAddFormComponent implements OnInit {
  @Input() todoList: ITodo[];
  newTodoTitle: string;

  constructor(
    private todoStoreService: TodoStoreService
  ) { }

  ngOnInit(): void { }

  addNewTodo(title: string): void {
    if (!title) {
      return;
    }

    const newTodoId = +this.todoList[this.todoList.length - 1].id + 1;
    const newTodo = {
      userId: 1,
      id: newTodoId,
      title,
      completed: false
    };

    this.todoStoreService.todos = [...this.todoStoreService.todos, newTodo];
    this.newTodoTitle = '';
  }
}
