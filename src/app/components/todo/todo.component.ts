import { Component, OnInit } from '@angular/core';

import { ITodo } from '../../interfaces/Todo';
import { TodoStoreService } from '../../services/todo-store.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  todoList: ITodo[];

  constructor(
    private todoStoreService: TodoStoreService
  ) { }

  ngOnInit(): void {
    this.todoStoreService.initTodoList();
    this.todoStoreService.todosWatcher.subscribe(todos => {
      if (todos) {
        this.todoList = todos;
      }
    }, err => console.error(err));
  }
}
