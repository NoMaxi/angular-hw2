import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ITodo } from '../interfaces/Todo';

@Injectable({
  providedIn: 'root',
})
export class TodoStoreService {
  private todoList: ITodo[] = [];
  private todosWatcherSource: BehaviorSubject<any> = new BehaviorSubject<any>(this.todos);
  public todosWatcher = this.todosWatcherSource.asObservable();

  constructor() { }

  public get todos(): ITodo[] {
    return this.todoList;
  }

  public set todos(todos: ITodo[]) {
    this.todoList = [...todos];
    this.todosWatcherSource.next([...todos]);
  }

  initTodoList(limit: number = 10): void {
    this.fetchData(limit)
      .then((data: ITodo[]) => {
        if (data) {
          this.todos = data;
        }
      }, err => console.error(err));
  }

  fetchData(limit: number): Promise<ITodo[]> {
    return fetch(
      `https://jsonplaceholder.typicode.com/todos?_limit=${limit}`
    ).then((response) => response.json());
  }
}
