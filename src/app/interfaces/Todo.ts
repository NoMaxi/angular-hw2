export interface ITodo {
  title: string;
  completed: boolean;
  userId: number;
  id: number;
}
